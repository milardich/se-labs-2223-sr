from query_handler_base import QueryHandlerBase
import random
import json
import requests

class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        return "chuck" in query
        pass

    def process(self, query):

        try:
            result = self.call_api()
            joke = result["value"]
            self.ui.say(f"{joke}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Chuck Norris api.")
            self.ui.say("Try something else!")
        pass

    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
            "accept": "application/json",
            "X-RapidAPI-Key": "e78200e0fbmshf1f0ec7a967c82ep194529jsn7c5672e6521e",
            "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)

        
        return json.loads(response.text)

from query_handler_base import QueryHandlerBase
import json
import requests

class DictionaryHandler(QueryHandlerBase):
    def can_process_query(self, query):
        return "dictionary" or "dict" in query
        pass

    def process(self, query):
        try:
            userInput = query.split()
            word = userInput[1]
            result = self.call_api(word)
            meaning = result["list"][0]["definition"]
            self.ui.say(f"{meaning}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact UrbanDictionary api.")
            self.ui.say("Try something else!")
        pass

    def call_api(self, word):
        url = "https://mashape-community-urban-dictionary.p.rapidapi.com/define"

        querystring = {"term":word}

        headers = {
            "X-RapidAPI-Key": "e78200e0fbmshf1f0ec7a967c82ep194529jsn7c5672e6521e",
            "X-RapidAPI-Host": "mashape-community-urban-dictionary.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        
        return json.loads(response.text)

import json

class Employee:
    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"

with open(".\lab2\ex4-employees.json", "r", encoding="utf-8") as f:
    employees = json.load(f)

#print(employees)
#print("\n\n")

employee_list = []

for employee in employees:
    temp = Employee(employee['employee'], employee['title'], employee['age'], employee['office'])
    #print(temp)
    employee_list.append(temp)

for e in employee_list:
    print(e)

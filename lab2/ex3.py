import json

csvfile = open(".\lab2\ex2-text.csv", "r")
csvfile.readline()

employees = []
dictEmployees = []

for line in csvfile:
    employees.append(line.split(","))

for employeeData in employees:
    dictEmployees.append(
        {
            'employee': employeeData[0],
            'title': employeeData[1],
            'age': employeeData[2],
            'office': employeeData[3]
        }
    )

print(dictEmployees)

with open('.\lab2\ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(dictEmployees, f)


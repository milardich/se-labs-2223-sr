userInput = input("Enter a string: ")
reversedInput = ''

for i in range(len(userInput)):
    reversedInput += userInput[len(userInput) - i - 1]

if userInput == reversedInput:
    print(userInput + " is a palindrome")
else:
    print(userInput + " is not a palindrome")


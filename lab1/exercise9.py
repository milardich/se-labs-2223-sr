import random
randNumber = random.randint(0,9)
#print("Generated number: " + str(randNumber))
while(1):
    userInput = int(input("Enter a number: "))
    if userInput > randNumber:
        print("Too high")
    elif userInput < randNumber:
        print("Too low")
    else:
        print("bingo")
        again = input("Play again? Y / N: ")
        if again == "Y":
            randNumber = random.randint(0,9)
            continue
        else:
            break

def extractNumbers(list):
    listOfStrNumbers = list.split(",")
    listOfIntegers = []

    for i in listOfStrNumbers:
        listOfIntegers.append(int(i))
    return listOfIntegers

def largestValue(list):
    max = list[0]
    for i in list:
        if i > max:
            max = i
    return max

def smallestValue(list):
    min = list[0]
    for i in list:
        if i < min:
            min = i
    return min

def centeredAverage(list):
    counter = 0
    sum = 0
    max = largestValue(list)
    min = smallestValue(list)

    for i in range(len(list)):
        if list[i] == max or list[i] == min:
            continue
        else:
            counter += 1
            sum = sum + list[i]
    return sum / counter

def main():
    userInput = input("Enter comma separated integers: ")
    extractedNumbers = extractNumbers(userInput)
    avg = centeredAverage(extractedNumbers)
    print("Average: " + str(avg))

if __name__ == "__main__":
    main()
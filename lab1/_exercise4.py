def char_types_count(_string):
    uppercaseCount = 0
    lowercaseCount = 0
    numberCount = 0
    
    for i in _string:
        if str.isnumeric(i):
            numberCount += 1
        elif str.isupper(i):
            uppercaseCount += 1
        elif str.islower(i):
            lowercaseCount +=1
        else:
            continue
    return (uppercaseCount, lowercaseCount, numberCount)        

def main():
    userInput = input("Enter a string: ")
    t_count = char_types_count(userInput)
    print("(upper, lower, numbers): " + str(t_count))

if __name__ == "__main__":
    main()
def extractNumbers(list):
    listOfStrNumbers = list.split(",")
    listOfIntegers = []

    for i in listOfStrNumbers:
        listOfIntegers.append(int(i))
    return listOfIntegers

def contains2nextto2(list):
    index = 0
    for i in list:
        if list[index] == 2 and list[index + 1] == 2:
            return True
        index += 1
    return False

def main():        
    userInput = input("Enter numbers: ")
    numbers = extractNumbers(userInput)
    contains = contains2nextto2(numbers)
    print(contains)

if __name__ == "__main__":
    main()
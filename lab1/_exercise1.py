def extractNumbers(list):
    listOfStrNumbers = list.split(",")
    listOfIntegers = []

    for i in listOfStrNumbers:
        listOfIntegers.append(int(i))
    return listOfIntegers

def evenNumbers(list):
    counter = 0
    for i in list:
        if i % 2 == 0:
            counter += 1
    return counter

def main():
    userInput = input("Enter comma separated integers: ")
    extractedNumbers = extractNumbers(userInput)
    n = evenNumbers(extractedNumbers)
    print("Number of evens: " + str(n))

if __name__ == "__main__":
    main()